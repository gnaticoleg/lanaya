FROM node:20.15.0

WORKDIR /app

COPY package*.json .

RUN npm ci

COPY . .

RUN npx prisma generate

CMD npm run start:dev
