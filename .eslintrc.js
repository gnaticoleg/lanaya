module.exports = {
  // Parser to make ESLint work with typescript
  parser: '@typescript-eslint/parser',

  // We designate the main .eslintrc file so that the linter does not look further
  root: true,

  // Specifying parser options
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2021,
  },

  // For which environments the script is intended
  env: {
    browser: true,
    node: true,
    jest: true,
    es2021: true,
  },

  // Tells ESLint to load installed plugins (we omit 'eslint-plugin' from the names).
  // This allows typescript-eslint rules to be used in code.
  plugins: ['@typescript-eslint', 'prettier', 'simple-import-sort', 'jest', 'json', 'import', 'node'],

  settings: {
    node: {
      tryExtensions: ['.js', '.json', '.node', '.ts', '.d.ts'],
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },

  // Tells ESLint that the configuration from plugins should be extended.
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'plugin:json/recommended-legacy',
    'plugin:security/recommended-legacy',
    'plugin:import/recommended',
    'plugin:node/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],

  rules: {
    '@typescript-eslint/no-explicit-any': 'warn',
    '@typescript-eslint/no-empty-interface': 'warn',
    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/explicit-function-return-type': 'warn',
    '@typescript-eslint/explicit-module-boundary-types': 'warn',
    '@typescript-eslint/no-unused-vars': 'warn',
    'simple-import-sort/imports': 'warn',
    'simple-import-sort/exports': 'error',
    'import/first': 'error',
    'import/newline-after-import': 'warn',
    'import/no-duplicates': 'error',
    'prefer-arrow-callback': 'warn',
    'prefer-const': 'error',
    quotes: ['error', 'single'],

    'node/no-unpublished-import': [
      'error',
      {
        allowModules: ['@nestjs/testing', 'supertest'],
      },
    ],

    'node/no-unsupported-features/es-syntax': [
      'error',
      {
        version: '>=13.0.0',
        ignores: ['modules'],
      },
    ],

    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        prefix: ['I'],
        format: ['PascalCase'],
      },
      {
        selector: 'typeAlias',
        prefix: ['T'],
        format: ['PascalCase'],
      },
      {
        selector: 'enum',
        suffix: ['Enum'],
        format: ['PascalCase'],
      },
      {
        selector: 'class',
        format: ['PascalCase'],
      },
    ],

    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
  },
};
