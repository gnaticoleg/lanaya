import { IsNotEmpty, IsString, IsUUID, MaxLength } from 'class-validator';

import CLASS_VALIDATOR_CONSTANT from '../../app/constants/class-validator.constant';

export default class CreateMessageDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.MESSAGE.CONTENT.MAX_LENGTH)
  content: string;

  @IsString()
  @IsUUID()
  chat_id: string;

  @IsString()
  @IsUUID()
  user_id: string;
}
