import { UseFilters, UsePipes, ValidationPipe } from '@nestjs/common';
import { MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';

import MESSAGE_EMITS from './constants/message.emits.constant';
import CreateMessageDto from './dto/create-message.dto';
import ValidationFilter from './filters/validation.filter';
import IMessage from './interfaces/message.interface';
import MessagesService from './messages.service';

// export class WsAuthGuard extends AuthGuard('jwt') {
//   canActivate(context: ExecutionContext): boolean {
//     const client = context.switchToWs().getClient();
//
//     console.log('client', client.handshake);
//     const authorization = client.handshake.headers['authorization'];
//     // Check the authorization header here
//     return !!authorization; // Simple check for demonstration
//   }
// }

// 1. Add WebScokets Rooms for emits (new message for each chat room)
// 2. Use guard
@WebSocketGateway()
// @UseGuards(WsAuthGuard)
@UseFilters(new ValidationFilter())
@UsePipes(new ValidationPipe())
export default class MessagesGateway {
  @WebSocketServer()
  server: Server;

  constructor(private readonly messagesService: MessagesService) {}

  @SubscribeMessage(MESSAGE_EMITS.CREATE)
  async create(@MessageBody() createMessageDto: CreateMessageDto): Promise<IMessage> {
    const { chat_id, user_id, content } = createMessageDto;

    const message: IMessage = await this.messagesService.create({
      chat_id,
      user_id,
      content,
    });

    this.server.emit(MESSAGE_EMITS.CREATED, message);

    return message;
  }
}
