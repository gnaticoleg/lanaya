import { Module } from '@nestjs/common';

import ChatsModule from '../chats/chats.module';
import PrismaService from '../prisma/prisma.service';
import MessagesGateway from './messages.gateway';
import MessagesService from './messages.service';

@Module({
  imports: [ChatsModule],
  providers: [MessagesService, MessagesGateway, PrismaService],
})
export default class MessagesModule {}
