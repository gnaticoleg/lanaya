const MESSAGE_HTTP = Object.freeze({
  CREATED: 'Message successfully created',
});

export default MESSAGE_HTTP;
