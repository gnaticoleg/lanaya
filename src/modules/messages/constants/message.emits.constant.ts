const MESSAGE_EMITS = Object.freeze({
  CREATE: '/create@message',
  CREATED: '/created@message',
});

export default MESSAGE_EMITS;
