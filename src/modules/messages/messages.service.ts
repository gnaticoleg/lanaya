import { BadRequestException, Injectable, Logger } from '@nestjs/common';

import ChatsService from '../chats/chats.service';
import CHAT_HTTP from '../chats/constants/chat-http.constant';
import IUserChat from '../chats/interfaces/chat-user.interface';
import PrismaService from '../prisma/prisma.service';
import MESSAGE_HTTP from './constants/message-http.constant';
import CreateMessageDto from './dto/create-message.dto';
import IMessage from './interfaces/message.interface';

@Injectable()
export default class MessagesService {
  private readonly logger: Logger = new Logger();

  constructor(
    private readonly chatsService: ChatsService,
    private readonly prisma: PrismaService
  ) {}

  async create(createMessageDto: CreateMessageDto): Promise<IMessage> {
    const context: string = 'MessagesService.create';

    const { chat_id, user_id, content } = createMessageDto;

    await this.chatsService.findOrThrow({ id: chat_id });

    const isUserInChat: IUserChat | null = await this.chatsService.checkIfUserInChat({ user_id, chat_id });

    if (!isUserInChat) {
      this.logger.error(CHAT_HTTP.NOT_IN_CHAT, context);
      throw new BadRequestException(CHAT_HTTP.NOT_IN_CHAT);
    }

    const createdMessage: IMessage = await this.prisma.message.create({
      data: {
        content,
        creator: { connect: { id: user_id } },
        chat: { connect: { id: chat_id } },
      },
    });

    this.logger.verbose(MESSAGE_HTTP.CREATED, context);
    return createdMessage;
  }
}
