interface IMessage {
  id: string;
  created_at: Date;
  content: string;
  creator_id: string;
  chat_id: string;
}

export default IMessage;
