import { BadRequestException, Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { Prisma } from '@prisma/client';

import passwordToHash from '../auth/utils/password-to-hash.util';
import PrismaService from '../prisma/prisma.service';
import USER_HTTP from './constants/user-http.constant';
import CreateUserRequestDto from './dto/create-user.request.dto';
import IUser from './interfaces/user.interface';
import TSafeUser from './types/safe-user.type';

@Injectable()
export default class UsersService {
  private readonly logger: Logger = new Logger();

  constructor(private readonly prisma: PrismaService) {}

  async find(condition: Prisma.UserWhereUniqueInput, getSafeUser: boolean = true): Promise<IUser | TSafeUser | null> {
    const user: IUser | TSafeUser | null = await this.prisma.user.findUnique({
      where: condition,
      omit: { password: getSafeUser },
    });
    return user;
  }

  async findOrThrow(condition: Prisma.UserWhereUniqueInput, getSafeUser: boolean = true): Promise<IUser | TSafeUser> {
    const context: string = 'UsersService.findOrThrow';

    const user: IUser | TSafeUser | null = await this.prisma.user.findUnique({
      where: condition,
      omit: { password: getSafeUser },
    });

    if (!user) {
      this.logger.error(USER_HTTP.NON_EXISTENCE, context);
      throw new UnauthorizedException(USER_HTTP.NON_EXISTENCE);
    }

    return user;
  }

  async create(createUserRequestDto: CreateUserRequestDto): Promise<TSafeUser> {
    const context: string = 'UsersService.create';

    const userByEmail: TSafeUser | null = await this.find({ email: createUserRequestDto.email });

    if (userByEmail) {
      this.logger.error(USER_HTTP.EXIST, context);
      throw new BadRequestException(USER_HTTP.EXIST);
    }

    const hashedPassword: string = await passwordToHash(createUserRequestDto.password);

    const userForCreateWithHashedPassword: CreateUserRequestDto = { ...createUserRequestDto, password: hashedPassword };

    const createdUser: TSafeUser = await this.prisma.user.create({
      data: userForCreateWithHashedPassword,
      omit: { password: true },
    });

    this.logger.verbose(USER_HTTP.CREATED, context);
    return createdUser;
  }

  async delete(user_id: string): Promise<void> {
    const context: string = 'UsersService.delete';

    await this.findOrThrow({ id: user_id });

    await this.prisma.user.delete({ where: { id: user_id } });

    this.logger.verbose(USER_HTTP.DELETED, context);
  }
}
