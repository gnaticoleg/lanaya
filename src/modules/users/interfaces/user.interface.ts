interface IUser {
  id: string;
  created_at: Date;
  updated_at: Date;
  email: string;
  first_name: string;
  last_name: string;
  age: number;
  password: string;
}

export default IUser;
