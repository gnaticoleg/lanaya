import TSafeUser from '../types/safe-user.type';

interface ISafeUserWithToken extends TSafeUser {
  access_token: string;
}

export default ISafeUserWithToken;
