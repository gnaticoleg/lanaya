import IUser from '../interfaces/user.interface';

type TSafeUser = Omit<IUser, 'password'>;

export default TSafeUser;
