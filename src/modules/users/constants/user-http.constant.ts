const USER_HTTP = Object.freeze({
  EXIST: 'User already exists',
  NON_EXISTENCE: 'Incorrect password or email',
  CREATED: 'User successfully created',
  DELETED: 'User successfully deleted',
  SING_IN: 'User successfully signed in',
  SIGN_UP: 'User successfully signed up',
});

export default USER_HTTP;
