import { createParamDecorator, ExecutionContext } from '@nestjs/common';

const CurrentUserId = createParamDecorator((argument: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  return request.user.sub;
});

export default CurrentUserId;
