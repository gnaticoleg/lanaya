export default class CreateUserRequestDto {
  email: string;
  first_name: string;
  last_name: string;
  age: number;
  password: string;
}
