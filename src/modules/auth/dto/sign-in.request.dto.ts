import { IsEmail, IsLowercase, IsNotEmpty, IsString, MaxLength } from 'class-validator';

import CLASS_VALIDATOR_CONSTANT from '../../app/constants/class-validator.constant';

export default class SignInRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @IsLowercase()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.USER.EMAIL.MAX_LENGTH)
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
