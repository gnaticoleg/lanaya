import {
  IsEmail,
  IsInt,
  IsLowercase,
  IsNotEmpty,
  IsString,
  IsStrongPassword,
  Max,
  MaxLength,
  Min,
} from 'class-validator';

import CLASS_VALIDATOR_CONSTANT from '../../app/constants/class-validator.constant';

export default class SignUpRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @IsLowercase()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.USER.EMAIL.MAX_LENGTH)
  email: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.USER.FIRST_NAME.MAX_LENGTH)
  first_name: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.USER.LAST_NAME.MAX_LENGTH)
  last_name: string;

  @IsNotEmpty()
  @IsInt()
  @Min(CLASS_VALIDATOR_CONSTANT.USER.AGE.MIN)
  @Max(CLASS_VALIDATOR_CONSTANT.USER.AGE.MAX)
  age: number;

  @IsNotEmpty()
  @IsString()
  @IsStrongPassword(CLASS_VALIDATOR_CONSTANT.USER.PASSWORD_OPTIONS, {
    message: CLASS_VALIDATOR_CONSTANT.USER.PASSWORD_REQUIREMENTS,
  })
  password: string;
}
