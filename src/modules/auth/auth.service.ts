import { Injectable, Logger } from '@nestjs/common';

import TokenService from '../token/token.service';
import USER_HTTP from '../users/constants/user-http.constant';
import ISafeUserWithToken from '../users/interfaces/safe-user-with-token.interface';
import IUser from '../users/interfaces/user.interface';
import TSafeUser from '../users/types/safe-user.type';
import UsersService from '../users/users.service';
import SignInRequestDto from './dto/sign-in.request.dto';
import SignUpRequestDto from './dto/sign-up.request.dto';
import matchPasswordOrThrow from './utils/match-password-or-throw.util';

@Injectable()
export default class AuthService {
  private readonly logger: Logger = new Logger();

  constructor(
    private readonly usersService: UsersService,
    private readonly tokenService: TokenService
  ) {}

  private async addTokenToUser(user: TSafeUser): Promise<ISafeUserWithToken> {
    const accessToken: string = await this.tokenService.generate(user);
    const userWithToken: ISafeUserWithToken = { ...user, access_token: accessToken };
    return userWithToken;
  }

  private async checkUserOrThrow(email: string, password: string): Promise<TSafeUser> {
    const userByEmail: IUser = (await this.usersService.findOrThrow({ email }, false)) as IUser;

    await matchPasswordOrThrow(password, userByEmail.password);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password: _, ...user } = userByEmail;

    return user;
  }

  async signUp(signUpRequestDto: SignUpRequestDto): Promise<ISafeUserWithToken> {
    const context: string = 'AuthService.signUp';

    const createdUser: TSafeUser = await this.usersService.create(signUpRequestDto);

    const createdUserWithToken: ISafeUserWithToken = await this.addTokenToUser(createdUser);

    this.logger.verbose(USER_HTTP.SIGN_UP, context);
    return createdUserWithToken;
  }

  async signIn(signInRequestDto: SignInRequestDto): Promise<ISafeUserWithToken> {
    const context: string = 'AuthService.signIn';

    const user: TSafeUser = await this.checkUserOrThrow(signInRequestDto.email, signInRequestDto.password);

    const userWithToken: ISafeUserWithToken = await this.addTokenToUser(user);

    this.logger.verbose(USER_HTTP.SING_IN, context);
    return userWithToken;
  }

  async delete(user_id: string): Promise<void> {
    const context: string = 'AuthService.delete';

    await this.usersService.delete(user_id);

    this.logger.verbose(USER_HTTP.DELETED, context);
  }
}
