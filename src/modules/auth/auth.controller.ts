import { Body, Controller, Delete, HttpCode, HttpStatus, Param, Post, UseGuards } from '@nestjs/common';

import IdParamDto from '../app/dto/params/id.param.dto';
import TokenGuard from '../token/guards/token.guard';
import USER_HTTP from '../users/constants/user-http.constant';
import ISafeUserWithToken from '../users/interfaces/safe-user-with-token.interface';
import AuthService from './auth.service';
import SignInRequestDto from './dto/sign-in.request.dto';
import SignUpRequestDto from './dto/sign-up.request.dto';

@Controller('auth')
export default class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('sign-up')
  async signUp(@Body() signUpRequestDto: SignUpRequestDto): Promise<ISafeUserWithToken> {
    const createdUser: ISafeUserWithToken = await this.authService.signUp(signUpRequestDto);
    return createdUser;
  }

  @Post('sign-in')
  @HttpCode(HttpStatus.OK)
  async signIn(@Body() signInRequestDto: SignInRequestDto): Promise<ISafeUserWithToken> {
    const loggedUser: ISafeUserWithToken = await this.authService.signIn(signInRequestDto);
    return loggedUser;
  }

  @UseGuards(TokenGuard)
  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  async delete(@Param() params: IdParamDto): Promise<string> {
    await this.authService.delete(params.id);
    return USER_HTTP.DELETED;
  }
}
