import bcrypt from 'bcrypt';

async function passwordToHash(password: string): Promise<string> {
  const saltRounds: number = parseInt(process.env.SALT_ROUNDS as string);
  const salt: string = await bcrypt.genSalt(saltRounds);
  const hashedPassword: string = await bcrypt.hash(password, salt);
  return hashedPassword;
}

export default passwordToHash;
