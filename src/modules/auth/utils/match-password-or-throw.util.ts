import { Logger, UnauthorizedException } from '@nestjs/common';
import bcrypt from 'bcrypt';

import USER_HTTP from '../../users/constants/user-http.constant';

async function matchPasswordOrThrow(password: string, hash: string): Promise<boolean> {
  const logger: Logger = new Logger();
  const context: string = 'utils.matchPasswordOrThrow';

  const isPasswordMatch: boolean = await bcrypt.compare(password, hash);

  if (!isPasswordMatch) {
    logger.error(USER_HTTP.NON_EXISTENCE, context);
    throw new UnauthorizedException(USER_HTTP.NON_EXISTENCE);
  }

  return isPasswordMatch;
}

export default matchPasswordOrThrow;
