import { BadRequestException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Prisma } from '@prisma/client';

import PaginationDto from '../app/dto/params/pagination.dto';
import PrismaService from '../prisma/prisma.service';
import UsersService from '../users/users.service';
import CHAT_HTTP from './constants/chat-http.constant';
import CreateChatDto from './dto/create-chat.dto';
import JoinChatDto from './dto/join-chat.dto';
import UpdateChatDto from './dto/update-chat.dto';
import IChat from './interfaces/chat.interface';
import IChatGetAll from './interfaces/chat-get-all.interface';
import IChatUser from './interfaces/chat-user.interface';

@Injectable()
export default class ChatsService {
  private readonly logger: Logger = new Logger();

  constructor(
    private readonly prisma: PrismaService,
    private readonly usersService: UsersService
  ) {}

  async findOrThrow(condition: Prisma.ChatWhereUniqueInput): Promise<IChat> {
    const context: string = 'ChatsService.findOrThrow';

    const foundChat: IChat | null = await this.prisma.chat.findUnique({ where: condition });

    if (!foundChat) {
      this.logger.error(CHAT_HTTP.NON_EXISTENCE, context);
      throw new NotFoundException(CHAT_HTTP.NON_EXISTENCE);
    }

    return foundChat;
  }

  async checkIfUserInChat({ user_id, chat_id }: { user_id: string; chat_id: string }): Promise<IChatUser | null> {
    const isUserInChat: IChatUser | null = await this.prisma.chatUser.findUnique({
      where: { user_id_chat_id: { user_id, chat_id } },
    });

    return isUserInChat;
  }

  async create(createChatDto: CreateChatDto, user_id: string): Promise<IChat> {
    const context: string = 'ChatsService.create';

    const createdChat: IChat = await this.prisma.chat.create({
      data: {
        title: createChatDto.title,
        description: createChatDto.description,
        creator: { connect: { id: user_id } },
        user_chat: { create: { user: { connect: { id: user_id } } } },
      },
    });

    this.logger.verbose(CHAT_HTTP.CREATED, context);
    return createdChat;
  }

  async read(chat_id: string): Promise<IChat> {
    const foundChat: IChat = await this.findOrThrow({ id: chat_id });
    return foundChat;
  }

  async getAll(paginationDto: PaginationDto): Promise<IChatGetAll> {
    const { page, limit } = paginationDto;

    const skip: number = (page - 1) * limit;

    const allChats: IChat[] | [] = await this.prisma.chat.findMany({
      skip,
      take: limit,
    });

    const total: number = await this.prisma.chat.count();

    return {
      total,
      page,
      limit,
      data: allChats,
    };
  }

  async update(chat_id: string, updateChatDto: UpdateChatDto): Promise<IChat> {
    const context: string = 'ChatsService.update';

    const chatById: IChat = await this.findOrThrow({ id: chat_id });

    const updatedChat: IChat = await this.prisma.chat.update({
      where: { id: chatById.id },
      data: updateChatDto,
    });

    this.logger.verbose(CHAT_HTTP.UPDATED, context);
    return updatedChat;
  }

  async delete(chat_id: string): Promise<void> {
    const context: string = 'ChatsService.delete';

    await this.findOrThrow({ id: chat_id });

    await this.prisma.chat.delete({ where: { id: chat_id } });

    this.logger.verbose(CHAT_HTTP.DELETED, context);
  }

  async join({ user_id, chat_id }: JoinChatDto): Promise<void> {
    const context: string = 'ChatsService.join';

    await this.findOrThrow({ id: chat_id });

    await this.usersService.findOrThrow({ id: user_id });

    const isUserInChat: IChatUser | null = await this.checkIfUserInChat({ user_id, chat_id });

    if (isUserInChat) {
      this.logger.error(CHAT_HTTP.ALREDY_IN_CHAT, context);
      throw new BadRequestException(CHAT_HTTP.ALREDY_IN_CHAT);
    }

    await this.prisma.chatUser.create({
      data: {
        user: { connect: { id: user_id } },
        chat: { connect: { id: chat_id } },
      },
    });

    this.logger.verbose(CHAT_HTTP.JOIN_CHAT, context);
  }

  async leave({ user_id, chat_id }: JoinChatDto): Promise<void> {
    const context: string = 'ChatsService.leave';

    const chatById: IChat = await this.findOrThrow({ id: chat_id });

    if (chatById.creator_id === user_id) {
      this.logger.error(CHAT_HTTP.CREATOR_CANT_LEAVE, context);
      throw new BadRequestException(CHAT_HTTP.CREATOR_CANT_LEAVE);
    }

    await this.usersService.findOrThrow({ id: user_id });

    const isUserInChat: IChatUser | null = await this.checkIfUserInChat({ user_id, chat_id });

    if (!isUserInChat) {
      this.logger.error(CHAT_HTTP.NOT_IN_CHAT, context);
      throw new BadRequestException(CHAT_HTTP.NOT_IN_CHAT);
    }

    await this.prisma.chatUser.delete({ where: { user_id_chat_id: { user_id, chat_id } } });

    this.logger.verbose(CHAT_HTTP.LEAVE_CHAT, context);
  }
}
