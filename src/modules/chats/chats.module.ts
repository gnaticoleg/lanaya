import { Module } from '@nestjs/common';

import PrismaService from '../prisma/prisma.service';
import UsersModule from '../users/users.module';
import ChatsController from './chats.controller';
import ChatsService from './chats.service';

@Module({
  imports: [UsersModule],
  providers: [ChatsService, PrismaService],
  controllers: [ChatsController],
  exports: [ChatsService],
})
export default class ChatsModule {}
