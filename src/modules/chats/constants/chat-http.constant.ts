const CHAT_HTTP = Object.freeze({
  NON_EXISTENCE: 'Chat does not exist',
  CREATED: 'Chat successfully created',
  UPDATED: 'Chat successfully updated',
  DELETED: 'Chat successfully deleted',
  JOIN_CHAT: 'User successfully added to the chats',
  LEAVE_CHAT: 'User successfully leave the chats',
  CREATOR_CANT_LEAVE: 'Chat creator can not leave the chats',
  ALREDY_IN_CHAT: 'User alredy in the chats',
  NOT_IN_CHAT: 'User not in the chats',
  PERMISSIONS_DENIED: 'Only the chats creator has access to this chats',
});

export default CHAT_HTTP;
