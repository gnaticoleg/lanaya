import IChat from './chat.interface';

interface IChatGetAll {
  total: number;
  page: number;
  limit: number;
  data: IChat[] | [];
}

export default IChatGetAll;
