interface IChatUser {
  created_at: Date;
  user_id: string;
  chat_id: string;
}

export default IChatUser;
