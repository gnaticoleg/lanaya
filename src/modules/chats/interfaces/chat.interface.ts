interface IChat {
  id: string;
  created_at: Date;
  updated_at: Date;
  title: string;
  description: string;
  creator_id: string;
}

export default IChat;
