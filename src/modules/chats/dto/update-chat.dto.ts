import { PartialType } from '@nestjs/swagger';

import CreateChatDto from './create-chat.dto';

export default class UpdateChatDto extends PartialType(CreateChatDto) {}
