import JoinChatDto from './join-chat.dto';

export default class LeaveChatDto extends JoinChatDto {}
