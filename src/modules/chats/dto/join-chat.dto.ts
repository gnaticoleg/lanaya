import { IsString, IsUUID } from 'class-validator';

export default class JoinChatDto {
  @IsString()
  @IsUUID()
  chat_id: string;

  @IsString()
  @IsUUID()
  user_id: string;
}
