import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

import CLASS_VALIDATOR_CONSTANT from '../../app/constants/class-validator.constant';

export default class CreateChatDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.CHAT.TITLE.MAX_LENGTH)
  title: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(CLASS_VALIDATOR_CONSTANT.CHAT.DESCRIPTION.MAX_LENGTH)
  description: string;
}
