import { CanActivate, ExecutionContext, ForbiddenException, Injectable, Logger } from '@nestjs/common';

import ChatsService from '../chats.service';
import CHAT_HTTP from '../constants/chat-http.constant';
import IChat from '../interfaces/chat.interface';

@Injectable()
export default class ChatCreatorGuard implements CanActivate {
  private readonly logger: Logger = new Logger();

  constructor(private readonly chatsService: ChatsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const user_id = request.user.sub;
    const chat_id = request.params.id;
    const chatById: IChat = await this.chatsService.read(chat_id);

    if (chatById.creator_id !== user_id) {
      this.logger.error(CHAT_HTTP.PERMISSIONS_DENIED, 'ChatCreatorGuard');
      throw new ForbiddenException(CHAT_HTTP.PERMISSIONS_DENIED);
    }

    return true;
  }
}
