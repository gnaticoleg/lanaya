import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';

import IdParamDto from '../app/dto/params/id.param.dto';
import PaginationDto from '../app/dto/params/pagination.dto';
import TokenGuard from '../token/guards/token.guard';
import CurrentUserId from '../users/decorators/current-user-id.decorator';
import ChatsService from './chats.service';
import CHAT_HTTP from './constants/chat-http.constant';
import CreateChatDto from './dto/create-chat.dto';
import JoinChatDto from './dto/join-chat.dto';
import LeaveChatDto from './dto/leave-chat.dto';
import UpdateChatDto from './dto/update-chat.dto';
import ChatCreatorGuard from './guards/chat-creator.guard';
import IChat from './interfaces/chat.interface';
import IChatGetAll from './interfaces/chat-get-all.interface';

@Controller('chats')
@UseGuards(TokenGuard)
export default class ChatsController {
  constructor(private readonly chatsService: ChatsService) {}

  @Post()
  async create(@Body() createChatDto: CreateChatDto, @CurrentUserId() user_id: string): Promise<IChat> {
    const createdChat: IChat = await this.chatsService.create(createChatDto, user_id);
    return createdChat;
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async all(@Query() paginationDto: PaginationDto): Promise<IChatGetAll> {
    const chats: IChatGetAll = await this.chatsService.getAll(paginationDto);
    return chats;
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async read(@Param() params: IdParamDto): Promise<IChat> {
    const foundChat: IChat = await this.chatsService.read(params.id);
    return foundChat;
  }

  @UseGuards(ChatCreatorGuard)
  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  async update(@Param() params: IdParamDto, @Body() updateChatDto: UpdateChatDto): Promise<IChat> {
    const updatedChat: IChat = await this.chatsService.update(params.id, updateChatDto);
    return updatedChat;
  }

  @UseGuards(ChatCreatorGuard)
  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  async delete(@Param() params: IdParamDto): Promise<string> {
    await this.chatsService.delete(params.id);
    return CHAT_HTTP.DELETED;
  }

  @Post('join')
  @HttpCode(HttpStatus.OK)
  async join(@Body() joinChatDto: JoinChatDto): Promise<string> {
    await this.chatsService.join(joinChatDto);
    return CHAT_HTTP.JOIN_CHAT;
  }

  @Post('leave')
  @HttpCode(HttpStatus.OK)
  async leave(@Body() leaveChatDto: LeaveChatDto): Promise<string> {
    await this.chatsService.leave(leaveChatDto);
    return CHAT_HTTP.LEAVE_CHAT;
  }
}
