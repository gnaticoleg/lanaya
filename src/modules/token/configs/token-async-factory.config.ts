import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModuleAsyncOptions, JwtModuleOptions } from '@nestjs/jwt';

const tokenAsyncFactoryConfig: JwtModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: async (confingService: ConfigService): Promise<JwtModuleOptions> => ({
    secret: confingService.get<string>('token.secret'),
    signOptions: { expiresIn: confingService.get<string>('token.expire') },
  }),
  inject: [ConfigService],
};

export default tokenAsyncFactoryConfig;
