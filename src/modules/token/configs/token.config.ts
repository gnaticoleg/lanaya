import { registerAs } from '@nestjs/config';

const tokenConfig = {
  secret: process.env.JWT_SECRET,
  expire: process.env.JWT_EXPIRE,
};

export default registerAs('token', () => tokenConfig);
