import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import TSafeUser from '../users/types/safe-user.type';

@Injectable()
export default class TokenService {
  constructor(private readonly jwtService: JwtService) {}

  async generate(user: TSafeUser): Promise<string> {
    const accessToken: string = await this.jwtService.signAsync({
      sub: user.id,
      user,
    });

    return accessToken;
  }
}
