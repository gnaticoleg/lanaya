import TSafeUser from '../../users/types/safe-user.type';

interface ITokenPayload {
  sub: string;
  user: TSafeUser;
  iat: number;
  exp: number;
}

export default ITokenPayload;
