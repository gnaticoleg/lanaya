import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import tokenAsyncFactoryConfig from './configs/token-async-factory.config';
import TokenStrategy from './stategies/token.strategy';
import TokenService from './token.service';

@Module({
  imports: [JwtModule.registerAsync(tokenAsyncFactoryConfig), PassportModule],
  providers: [TokenService, TokenStrategy],
  exports: [TokenService],
})
export default class TokenModule {}
