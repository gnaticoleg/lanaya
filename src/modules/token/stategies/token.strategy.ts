import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import ITokenPayload from '../interfaces/token-payload.interface';

@Injectable()
export default class TokenStrategy extends PassportStrategy(Strategy, 'lanaya-stategy') {
  constructor(configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('token.secret'),
    });
  }

  async validate(payload: ITokenPayload): Promise<ITokenPayload> {
    return payload;
  }
}
