import { Module } from '@nestjs/common';
import { ConfigModule, ConfigModuleOptions } from '@nestjs/config';

import appConfig from '../../configs/app/app.config';
import AuthModule from '../auth/auth.module';
import ChatsModule from '../chats/chats.module';
import MessagesModule from '../messages/messages.module';
import tokenConfig from '../token/configs/token.config';
import TokenModule from '../token/token.module';
import UsersModule from '../users/users.module';

const configModuleOptions: ConfigModuleOptions = {
  isGlobal: true,
  load: [appConfig, tokenConfig],
  cache: true,
};

@Module({
  imports: [
    ConfigModule.forRoot(configModuleOptions),
    AuthModule,
    TokenModule,
    UsersModule,
    ChatsModule,
    MessagesModule,
  ],
})
export default class AppModule {}
