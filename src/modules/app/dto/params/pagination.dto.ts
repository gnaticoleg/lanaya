import { Type } from 'class-transformer';
import { IsInt, IsOptional, Max, Min } from 'class-validator';

import CLASS_VALIDATOR_CONSTANT from '../../constants/class-validator.constant';

export default class PaginationDto {
  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Min(CLASS_VALIDATOR_CONSTANT.PAGINATION.PAGE.MIN)
  page: number = 1;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @Min(CLASS_VALIDATOR_CONSTANT.PAGINATION.LIMIT.MIN)
  @Max(CLASS_VALIDATOR_CONSTANT.PAGINATION.LIMIT.MAX)
  limit: number = 20;
}
