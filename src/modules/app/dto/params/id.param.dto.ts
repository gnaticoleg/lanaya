import { IsString, IsUUID } from 'class-validator';

export default class IdParamDto {
  @IsString()
  @IsUUID()
  id: string;
}
