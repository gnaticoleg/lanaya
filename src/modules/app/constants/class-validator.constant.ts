const CLASS_VALIDATOR_CONSTANT = Object.freeze({
  PAGINATION: {
    PAGE: { MIN: 1 },
    LIMIT: { MIN: 1, MAX: 100 },
  },
  CHAT: {
    TITLE: { MAX_LENGTH: 50 },
    DESCRIPTION: { MAX_LENGTH: 255 },
  },
  MESSAGE: { CONTENT: { MAX_LENGTH: 255 } },
  USER: {
    EMAIL: { MAX_LENGTH: 100 },
    FIRST_NAME: { MAX_LENGTH: 50 },
    LAST_NAME: { MAX_LENGTH: 50 },
    AGE: { MIN: 18, MAX: 100 },
    PASSWORD_OPTIONS: {
      minLength: 5,
      maxLength: 255,
      minUppercase: 1,
      minSymbols: 1,
      minNumbers: 1,
    },
    PASSWORD_REQUIREMENTS:
      'The password should be at least 5 characters long, contain at least 1 uppercase letter, 1 symbol, and 1 number.',
  },
});

export default CLASS_VALIDATOR_CONSTANT;
