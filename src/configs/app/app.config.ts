import { registerAs } from '@nestjs/config';

const appConfig = {
  port: parseInt(process.env.APP_PORT as string, 10) || 3000,
};

export default registerAs('app', () => appConfig);
