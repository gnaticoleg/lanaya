import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

import AppModule from './modules/app/app.module';

async function bootstrap(): Promise<void> {
  const log: Logger = new Logger('main.ts');

  const app: NestExpressApplication = await NestFactory.create<NestExpressApplication>(AppModule);

  const configService: ConfigService = app.get<ConfigService>(ConfigService);

  const APP_PORT: number = configService.get<number>('app.port', 3000);

  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  await app.listen(APP_PORT, () => log.verbose(`App running on port: ${APP_PORT}`));
}

bootstrap();
