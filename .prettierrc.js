module.exports = {
  printWidth: 120, // The length of the string that prettier will wrap ,

  tabWidth: 2, // Number of spaces per indent level.,

  semi: true, // Semicolons at the end of statements ,

  singleQuote: true, // Single quotes instead of double quotes singleQuote: true ,

  trailingComma: 'es5',// Trailing commas wherever possible. As an example arrays, objects ,

  arrowParens: 'always', // Leave parentheses at the only parameter of the arrow function, for typing ,

  endOfLine: 'auto',   // Maintain existing line endings (mixed values within one file are normalized by looking at what's used after the first line).

  bracketSpacing: true, // Print spaces between brackets in object literals.
};
